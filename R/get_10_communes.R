#' Recuperer toutes les ICPE de 10 communes a la fois
#' @param query_str_com character, liste des codes communes de la requete, separes par '%2C'
#'
#' @importFrom httr GET content
#' @importFrom jsonlite fromJSON
#' @importFrom purrr map_dfr pluck
#' @return
#' Un dataframe des ICPE localisees dans les communes listees dans `query_str_com` .
#' @export
#'
#' @examples
#' get_10_communes("35013%2C35045")

get_10_communes <- function(query_str_com = "35013%2C35045"){
  r <- httr::GET(paste0("https://www.georisques.gouv.fr/api/v1/installations_classees?code_insee=", query_str_com, '&page_size=100'), 
                 config = list(accept_encoding = "UTF-8", forbid_reuse = 1))
  nb_pages <- httr::content(r)$total_pages
  num_pages <- 1:nb_pages
  
  data <- suppressMessages(
    purrr::map_dfr(.x = num_pages,
                   .f = ~ httr::GET(paste0("https://www.georisques.gouv.fr/api/v1/installations_classees?code_insee=", query_str_com, '&page_size=100&page=', .x),
                                    config = list(accept_encoding = "UTF-8", forbid_reuse = 1)) %>% 
                     httr::content("text") %>% 
                     jsonlite::fromJSON(flatten = TRUE) %>% 
                     purrr::pluck("data"))
  )
  return(data)
}
