# icpe.georisques.api 0.0.0.9000 :baby:
1ere version du package.  
Intégration des fonctions `get_icpe_dep()` et `get_icpe_reg()` qui permettent de récupérer la liste des installations d'une région, ou d'un département au format sf.  
Possibilité de filtrer selon une ou plusieurs rubrique ICPE.  
