
# icpe.georisques.api

<!-- badges: start -->

[![pipeline
status](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/icpe.georisques.api/badges/main/pipeline.svg)](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/icpe.georisques.api/-/commits/master)
<!-- badges: end -->

Le package R `{icpe.georisques.api}` facilite la collecte de données sur les ICPE (installations classées pour la protection de l'environnement) depuis l'[API de géorisques](https://www.georisques.gouv.fr/doc-api#!/Installations32Class233es) avec R.


## Installation

Il s’installe depuis la forge gitlab du ministère de l’écologie.

``` r
remotes::install_gitlab(repo = "dreal-pdl/csd/icpe.georisques.api", host = "gitlab-forge.din.developpement-durable.gouv.fr")
```

## Exemple

``` r
library(icpe.georisques.api)
get_icpe_reg('52')

```

## Présentation du package

Lien vers le site pkgdown de présentation du package :  
<https://dreal-pdl.gitlab-pages.din.developpement-durable.gouv.fr/csd/icpe.georisques.api/index.html>

## Signalement de bugs ou demandes d’amélioration

Les bugs ou les demandes d’amélioration sont les bienvenues :
<https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/icpe.georisques.api/-/issues>
